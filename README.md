# GraphQL Fun
A small 3D game with React and GraphQL

## Run
```./run.sh```

## Deploy

- Create a Heroku app, place the repo to ```/heroku```
- ```./deploy.sh```

## What's behind?
- Recioto della Valpolicella
- ReactJS
- BabylonJS
- Apollo GraphQL