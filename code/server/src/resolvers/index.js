import getStorage from '../utils/getStorage';
import getPubSub from '../utils/getPubSub';


const storage = getStorage();
const pubsub = getPubSub();

export default {
  Query: {
    balls: (root, args, context, info) => {
      return storage.get('balls');
    }
  },
  Mutation: {
    addBall: (root, { ballData }, context, info) => {
      const balls = storage.get('balls');
      
      const newBall = {...ballData};
      balls.push(newBall);
      storage.set('balls', balls);

      pubsub.publish('BALL_ADDED', { ballAdded: newBall });

      return true;
    }
  },
  Subscription: {
    ballAdded: {
      subscribe: () => pubsub.asyncIterator(['BALL_ADDED']),
    }
  }
};
