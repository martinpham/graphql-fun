const path = require('path');

const webpackMerge = require('webpack-merge');
const webpackNodeExternals = require('webpack-node-externals');


const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const common = require('../common/config.js');

module.exports = webpackMerge.smart(common, {
  entry: [path.join(__dirname, '../../src/index.js')],
  externals: [
    webpackNodeExternals()
  ],
  mode: 'production',
  plugins: [
    new CleanWebpackPlugin(),
  ]
});