rm -rf dist
mkdir dist
cd server && yarn && yarn build && cp -rv build/* ../dist && cp -rv package.json ../dist/
cd ..
mkdir dist/public
cd client && yarn && yarn build && cp -rv build/* ../dist/public
cd ..