import React from 'react';
import * as BABYLON from 'babylonjs';
import * as OIMO from 'oimo';
import ballTexture from './textures/ball.jpg';

window.OIMO = OIMO; // not es6 friendly

export default class Scene extends React.Component {
    canvas = null
    engine = null
    scene = null
    // camera = null
    shadowGenerator = null
    physicsEngine = null
    ground = null
    ballMaterial = null
    border0 = null
    border1 = null
    border2 = null
    border3 = null
    light = null
    balls = []

    onCanvasRef = (canvas) => {
      console.log('> onCanvasRef');
      if (canvas !== null) {
        this.canvas = canvas;
      }
    }

    componentDidMount() {
      console.log('> componentDidMount');
      this.engine = new BABYLON.Engine(
          this.canvas, 
          true
      );

      this.scene = new BABYLON.Scene(this.engine);

      console.log('> setupEnvironment');
      // this.camera = new BABYLON.FreeCamera("camera0", new BABYLON.Vector3(-20, 20, -20), this.scene);
      // this.camera.attachControl(this.canvas, true);
      // this.camera.checkCollisions = true;
      // this.camera.applyGravity = true;
      // this.camera.setTarget(new BABYLON.Vector3(0, 0, 0));
  
      this.light = new BABYLON.DirectionalLight("light0", new BABYLON.Vector3(0.2, -1, 0), this.scene);
      this.light.position = new BABYLON.Vector3(0, 80, 0);
  
      // Material
      this.ballMaterial = new BABYLON.StandardMaterial("ballTexture", this.scene);
      this.ballMaterial.diffuseTexture = new BABYLON.Texture(ballTexture, this.scene);
      this.ballMaterial.emissiveColor = new BABYLON.Color3(0.5, 0.5, 0.5);
      this.ballMaterial.diffuseTexture.uScale = 5;
      this.ballMaterial.diffuseTexture.vScale = 5;
  
  
      // Shadows
      this.shadowGenerator = new BABYLON.ShadowGenerator(2048, this.light);
      this.shadowGenerator.useBlurExponentialShadowMap = true;
      this.shadowGenerator.useKernelBlur = true;
      this.shadowGenerator.blurKernel = 32;
  
      // Physics
      this.physicsEngine = this.scene.enablePhysics(new BABYLON.Vector3(0, -50, 0), new BABYLON.OimoJSPlugin());
  
      // Playthis.ground
      this.ground = BABYLON.Mesh.CreateBox("ground", 1, this.scene);
      this.ground.scaling = new BABYLON.Vector3(100, 1, 100);
      this.ground.position.y = -5.0;
      this.ground.checkCollisions = true;
  
      this.border0 = BABYLON.Mesh.CreateBox("border0", 1, this.scene);
      this.border0.scaling = new BABYLON.Vector3(1, 100, 100);
      this.border0.position.y = -5.0;
      this.border0.position.x = -50.0;
      this.border0.checkCollisions = true;
  
      this.border1 = BABYLON.Mesh.CreateBox("border1", 1, this.scene);
      this.border1.scaling = new BABYLON.Vector3(1, 100, 100);
      this.border1.position.y = -5.0;
      this.border1.position.x = 50.0;
      this.border1.checkCollisions = true;
  
      this.border2 = BABYLON.Mesh.CreateBox("border2", 1, this.scene);
      this.border2.scaling = new BABYLON.Vector3(100, 100, 1);
      this.border2.position.y = -5.0;
      this.border2.position.z = 50.0;
      this.border2.checkCollisions = true;
  
      this.border3 = BABYLON.Mesh.CreateBox("border3", 1, this.scene);
      this.border3.scaling = new BABYLON.Vector3(100, 100, 1);
      this.border3.position.y = -5.0;
      this.border3.position.z = -50.0;
      this.border3.checkCollisions = true;
  
      const groundMat = new BABYLON.StandardMaterial("groundMat", this.scene);
      groundMat.diffuseColor = new BABYLON.Color3(0.5, 0.5, 0.5);
      groundMat.emissiveColor = new BABYLON.Color3(0.2, 0.2, 0.2);
      groundMat.backFaceCulling = false;
      this.ground.material = groundMat;
      this.border0.material = groundMat;
      this.border1.material = groundMat;
      this.border2.material = groundMat;
      this.border3.material = groundMat;
      this.ground.receiveShadows = true;
  
      // Physics
      this.ground.physicsImpostor = new BABYLON.PhysicsImpostor(this.ground, BABYLON.PhysicsImpostor.BoxImpostor, { mass: 0, friction: 0.5, restitution: 0.7 }, this.scene);
      this.border0.physicsImpostor = new BABYLON.PhysicsImpostor(this.border0, BABYLON.PhysicsImpostor.BoxImpostor, { mass: 0 }, this.scene);
      this.border1.physicsImpostor = new BABYLON.PhysicsImpostor(this.border1, BABYLON.PhysicsImpostor.BoxImpostor, { mass: 0 }, this.scene);
      this.border2.physicsImpostor = new BABYLON.PhysicsImpostor(this.border2, BABYLON.PhysicsImpostor.BoxImpostor, { mass: 0 }, this.scene);
      this.border3.physicsImpostor = new BABYLON.PhysicsImpostor(this.border3, BABYLON.PhysicsImpostor.BoxImpostor, { mass: 0 }, this.scene);
  

      this.vrHelper = this.scene.createDefaultVRExperience();
      this.camera().ellipsoid = new BABYLON.Vector3(3, 16, 3);
  		this.camera().position = new BABYLON.Vector3(-20, 20, -20);
      this.camera().setTarget(new BABYLON.Vector3(0, 0, 0));
      this.camera().checkCollisions = true;
      this.camera().applyGravity = true;
      
      

      console.log('> setupEnvironment finished');

      this.balls = [...this.props.balls];

      this.addBalls(this.balls)
  
      this.engine.runRenderLoop(() => {
          if (this.scene) {
            this.scene.render();
          }
      });
      
    }

    camera = () => {
      return (this.vrHelper && this.vrHelper.currentVRCamera) || null;
    }
        
    addBalls = (balls) => {
      console.log('> addBalls');
      for(let ball of balls)
      {
        this.addBall(
          ball.x,
          ball.y,
          ball.z,
          ball.size
        );
      }
    }

    addBall = (x, y, z, size) => {
      console.log('> addBall', x, y, z, size);
      const sphere = BABYLON.Mesh.CreateSphere("Sphere0", 16, size, this.scene);
      sphere.material = this.ballMaterial;

      sphere.position = new BABYLON.Vector3(x, y, z);

      this.shadowGenerator.addShadowCaster(sphere);

      sphere.physicsImpostor = new BABYLON.PhysicsImpostor(sphere, BABYLON.PhysicsImpostor.SphereImpostor, { mass: 1 }, this.scene);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
      console.log('> componentDidUpdate');
      const balls = [...this.props.balls];

      if(balls.length > this.balls.length)
      {
        const newBalls = balls.splice(this.balls.length, balls.length - this.balls.length);
        this.balls = [...this.props.balls];

        this.addBalls(newBalls);
      }
    }
    
    render() {
      const { width, height, onClick } = this.props;

      return (
        <canvas
          onClick={onClick}
          width={width}
          height={height}
          ref={this.onCanvasRef}
        />
      )
    }
}