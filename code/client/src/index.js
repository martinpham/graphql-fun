import React from 'react';
import ReactDOM from 'react-dom';
import GraphQLProvider from './GraphQLProvider';
import App from './App';
import * as serviceWorker from './serviceWorker';
import "./index.scss";

const isSecure = window.location.protocol === 'https:';
const graphqlHost = window.location.hostname;
const graphqlPort = (process.env.NODE_ENV === 'development') ? 80 : window.location.port;

ReactDOM.render((
  <GraphQLProvider 
    graphqlEndpoint={`${isSecure ? 'https' : 'http'}://${graphqlHost}:${graphqlPort}/graphql`}
    graphqlSubscriptionEndpoint={`${isSecure ? 'wss' : 'ws'}://${graphqlHost}:${graphqlPort}/graphql`}
  >
    <App />
  </GraphQLProvider>
), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
