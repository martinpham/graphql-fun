import React, {useState} from 'react';
import innerHeight from 'ios-inner-height';
import Scene from './Scene';

import {useQuery, useMutation, useSubscription} from '@apollo/react-hooks';
import gql from 'graphql-tag';

export default () => {
  const [balls, setBalls] = useState([]);

  useQuery(gql`
    query {
      balls {
        x,
        y,
        z,
        size
      }
    }
  `, {
    onCompleted: (data) => {
      setBalls([...data.balls])
    }
  });

  const [addBall, addBallResult] = useMutation(gql`
    mutation addBall($x: Int, $y: Int, $z: Int, $size: Int){
      addBall(ballData: { x: $x, y: $y, z: $z, size: $size})
    }
  `);

  useSubscription(gql`
    subscription {
      ballAdded {
        x,
        y,
        z,
        size
      }
    }
  `, {
    onSubscriptionData: ({ subscriptionData }) => {
      const newBall = subscriptionData.data.ballAdded;

      setBalls(balls => {
        const newBalls = [...balls];
        newBalls.push(newBall);
        return newBalls;
      })
    }
  });


  return (
    <Scene
      width={window.innerWidth}
      height={innerHeight()}
      balls={balls}
      onClick={() => addBall({
        variables: {
          x: Math.floor(Math.random() * 5),
          y: Math.floor(Math.random() * 10),
          z: Math.floor(Math.random() * 15),
          size: 1 + Math.floor(Math.random() * 3),
        }
      })}
    />
  );
}

