bash build.sh
cd heroku && ls -pA | grep -v .git/ | grep -v app.json | grep -v Procfile | grep -v .gitignore | xargs rm -rf && cp -rv ../code/dist/* ./ && sed -i '' 's/scripts/_scripts/g' package.json && git add -A && git commit -m 'ok' && git push heroku master && cd ..
